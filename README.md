# Kata de la Lotería

En TuLotero nos han informado de que va a salir un nuevo tipo de sorteo, la Mini-Loteria. Queremos añadir este nuevo juego a las opciones de nuestro usuarios, así que nos repartimos el trabajo. A ti y a tu compañero os toca la parte del cálculo de premios.

Las reglas para el cálculo de premios son:

- Debemos poder guardar el número premiado del sorteo y obtenerlo como un String de 4 digitos.

- El sorteo consta de números desde el 1 hasta el 9999, ningún número fuera de ese rango es válido y si intentamos guardarlo o comprobar un dentro de ese rango devolveremos `null`.

- El resultado del sorteo será un único número. Si se acierta el número el premio será de 10.000€

- Todos los números dentro de la centena del número premiado tendrán 500€ de premio (por ejemplo, si el número premiado es el 1347 todos los números desde el 1300 hasta el 1399 tendrán 500€ de premio. Excepto el 1347 que ya tiene los 10.000€)

- Todos los números acabados en la misma cifra que el número premiado tendrán el reintegro del importe del boleto, 10€. Además, si el boleto ya está premiado se acumula el premio, excepto para el premio de 10.000€ que no se puede acumular. (por ejemplo, el número premiado es el 1347 y nosotros tenemos el 1307, el premio será 500€ por estar en la misma centena que el número premiado y 10€ del reintegro; un total de 510€ de premio)
